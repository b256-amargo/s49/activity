
// DISPLAYING / VIEWING POSTS

fetch("https://jsonplaceholder.typicode.com/posts")
.then(result => result.json())
.then(data => showPost(data));

const showPost = (fetchedPosts) => {
	let postEntries = "";
	fetchedPosts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector("#div-post-entries").innerHTML = postEntries;
};

// ----------------------------

// CREATING / ADDING POSTS

document.querySelector("#form-add-post").addEventListener("submit", (func) => {
	func.preventDefault();

	fetch("https://jsonplaceholder.typicode.com/posts", {
		method: "POST",
		body: JSON.stringify({
			title: document.querySelector("#txt-title").value,
			body: document.querySelector("#txt-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	// if fetched only for displaying (GET method), 2nd argument not needed
	.then(result => result.json())
	.then(data => {
		console.log(data);
		alert("Successfully Added!");
		document.querySelector("#txt-title").value = null; // for clearing out the form after creation
		document.querySelector("#txt-body").value = null; // for clearing out the form after creation
	});
});

// ----------------------------

// EDITING POSTS

const editPost = (postId) => {
	let title = document.querySelector(`#post-title-${postId}`).innerHTML;
	let body = document.querySelector(`#post-body-${postId}`).innerHTML;

	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;
	document.querySelector("#txt-edit-id").value = postId;
	document.querySelector("#btn-submit-update").removeAttribute("disabled");
};

// ----------------------------

// UPDATING POSTS AFTER EDITING

document.querySelector("#form-edit-post").addEventListener("submit", (func) => {
	func.preventDefault();
	fetch("https://jsonplaceholder.typicode.com/posts/1", {
	// posts/1 only as placeholder since fetched data is external and thus cannot actually be updated
		method: "PUT",
		body: JSON.stringify({
			id: document.querySelector("#txt-edit-id").value,
			title: document.querySelector("#txt-edit-title").value,
			body: document.querySelector("#txt-edit-body").value,
			userId: 1
		}),
		headers: {
			"Content-Type": "application/json"
		}
	})
	.then(result => result.json())
	.then(data => {
		console.log(data);
		alert("Successfully Updated!");
		document.querySelector("#txt-edit-id").value = null;
		document.querySelector("#txt-edit-title").value = null;
		document.querySelector("#txt-edit-body").value = null;
		document.querySelector("#btn-submit-update").setAttribute("disabled", true);
	});
});

// ----------------------------

// DELETING POSTS

const deletePost = (postId) => {
	fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
		method: "DELETE"
	});
	// for removing on the "database"

	let post = document.querySelector(`#post-${postId}`);
	post.remove();
	// for removing on the HTML DOM
};